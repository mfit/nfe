===
nfe
===

Collection of scripts to facilitate transformation and generation of excel sheets.

Options
-------

- basedir

- input file ( default in basedir / ... )
- output file ( default in basedir / ... )


- report missing files / halt on missing files
- add drawing header (output)
- add drawing header for empty (output)
- do reverse check ( are all partfiles in folder referenced) ?

- use auto mapping (hard codedy by col index) as fallback
