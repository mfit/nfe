from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages = ['xlrd', 'nfe'], excludes = [], )

import sys
base = 'Win32GUI' if sys.platform=='win32' else None

executables = [
    Executable('nfe/onefilegui.py', base=base, targetName = 'eteile-gui.exe', )
]

setup(name='ersatz-gui',
      version = '0.1',
      description = 'Ersatzteillisten',
      options = dict(build_exe = buildOptions),
      executables = executables)
