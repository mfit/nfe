#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Tkinter, Tkconstants, tkFileDialog
import ScrolledText
import logging
from ersatz import NFETeileProcess

class TextHandler(logging.Handler):
    """This class allows you to log to a Tkinter Text or ScrolledText widget"""
    def __init__(self, text):
        # run the regular Handler __init__
        logging.Handler.__init__(self)

        # Store a reference to the Text it will log to
        self.text = text

    def emit(self, record):
        msg = self.format(record)
        def append():
            self.text.configure(state='normal')
            self.text.insert(Tkinter.END, msg + '\n')
            self.text.configure(state='disabled')
            # Autoscroll to the bottom
            self.text.yview(Tkinter.END)
        # This is necessary because we can't modify the Text from other threads
        self.text.after(0, append)


class TkFileDialogExample(Tkinter.Frame):

    def __init__(self, root, logger=None):

        Tkinter.Frame.__init__(self, root)

        # options for buttons
        button_opt = {'fill': Tkconstants.BOTH, 'padx': 5, 'pady': 5}

        # define buttons
        Tkinter.Button(self, text='Choose directory', command=self.askdirectory).pack(**button_opt)

        # defining options for opening a directory
        self.dir_opt = options = {}
        options['initialdir'] = 'C:\\'
        options['mustexist'] = False
        options['parent'] = root
        options['title'] = 'Choose directory...'

        self.logger = logger

    def askdirectory(self):
        """Returns a selected directoryname."""
        theDir = tkFileDialog.askdirectory(**self.dir_opt)
        self.logger.info(theDir)
        self.do_process(theDir)
        return theDir

    def do_process(self, basedir):
        p = NFETeileProcess(logging.getLogger())
        p.set_basedir(basedir)
        p.set_dir_setup('simple')

        # p.src_startrow = args.startrow if args.startrow != None else 2
        # p.write_header = True if args.headers else False
        # p.subheaders = False if args.drawingheader == 'skip' else True
        # p.skip_empty_subheaders = True if args.drawingheader == 'nonempty' else False

        if not p.dirchecks():
            return False

        if not p.detect_mainfile():
            return False

        p.find_stklsts()
        p.get_outrows()
        p.write_outrows()
        return True

if __name__=='__main__':
    root = Tkinter.Tk()
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()

    TkFileDialogExample(root, logger).pack()

    st = ScrolledText.ScrolledText(root, state='disabled')
    st.configure(font='TkFixedFont')
    st.pack()

    # Create textLogger + Add the handler to logger
    text_handler = TextHandler(st)
    text_handler.setLevel(logging.DEBUG)
    logger.addHandler(text_handler)

    root.mainloop()
