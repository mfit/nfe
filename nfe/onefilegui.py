#!/usr/bin/env python
# -*- coding: utf-8 -*-

import Tkinter, Tkconstants, tkFileDialog
import ScrolledText
import logging

#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Ersatzteile
 -----------

Description:

 - from main project file, collect drawing names / part-lists
 - match drawing name to part-lists
 - read part list, filter relevant rows ( all E-types )
 - map into output format

 Notes:

    - 3 directories : DIR_ZEICHN, DIR_STKS, DIR_OUT
    - main project file :
        - a file in DIR_ZEICHN
        - 1st col contains file-handle, 2nd col contains drawing name
    - DIR_STKS contains part-lists
        - matching of handle / part-list filenames : discard part after underscore '_', or attempt guessing
        - if file is missing, issue at least warning
        - E-parts : "Type" col has type that *ends with 'E'*
    - mapping into outfile:
        Position = POs
        Quantity
        Benennung = Designation 1 (Benennung)
        Typ- und Normbez. Werkstoff = Designation 2
        Hersteller =  Remarks/Manufacturer
        Bestell. u. Serien Nr. Abmessungen = Dimension

        Zwischenheader der Zeichnung optional
            2. Feld Zwischenheader = ???

        Formatierung wenn möglich

# default stklst headers (en) :
[u'Artikelnummer', u'Gewicht', u'InnenL', u'InnenB', u'Typ', u'Pos.', u'Quan.', u'Type', u'Article number SWX', u'Designation 1', u'Designation 2', u'Dimension', u'Stand.', u'Material', u'Remarks/Manufacturer', u'Wt [kg]']


Notes on xlrd package:

    - sheet.col_slice(colx, start_rowx=0, end_rowx=None) # get one column
    - print book.nsheets # print number of sheets
    - print book.sheet_names() # print sheet names


"""

import csv
import xlrd
import os
import argparse
import sys
import logging

# from helper import UnicodeWriter
import csv
import cStringIO, codecs

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


# from nfe import __version__

# __author__ = "mf"
# __copyright__ = "mf"
# __license__ = "none"

_logger = logging.getLogger(__name__)

def gather_stklsts(dirname):
    """ gather stklst files and the (probable) drawing name
        make a list of files extracting the part before first dot, and
        the part before first underscore .

        Note: convert handle to unicode for later comparison to cell-contents

        TODO: search behaviour might be improved, if too strict this way
    """
    for f in os.listdir(dirname):
        handle = f.split('.')[0].split('_')[0]
        if handle:
            yield ((handle.decode('utf-8'), f))


def match_stklst(drawing, files):
    """ from nane of drawing, and strklst-structure, return suitable file, or False """
    matches = map(lambda r: r[1], filter(lambda r: r[0] == drawing, files))
    if len(matches):
        return matches[0]
    else:
        return False


def get_dyn_mapping(headers):
    """
        Mapping from stklst row to destination row
        Find mapping setup from headers.
        [u'Artikelnummer',
        u'Gewicht',
        u'InnenL',
        u'InnenB',
        u'Typ',
        u'Pos.',
        u'Quan.',
        u'Type',
        u'Article number SWX',
        u'Designation 1',
        u'Designation 2',
        u'Dimension',
        u'Stand.',
        u'Material',
        u'Remarks/Manufacturer',
        u'Wt [kg]']
    """
    try:
        mapping = {'type': headers.index('Type'),
                   'pos': headers.index('Pos.'),
                   'des1': headers.index('Designation 1'),
                   'des2': headers.index('Designation 2'),
                   'qt': headers.index('Quan.'),
                   'remarks': headers.index('Remarks/Manufacturer'),
                   'dim': headers.index('Dimension'),
                   }
    except ValueError as e:
        return (False, e)

    return (True, mapping)


def get_dest_mapper(mapping, drawing=''):
    """ Creates a mapper function from the map to create one row of output"""
    return lambda r: [
        int(r[mapping['pos']]),
        int(r[mapping['qt']]),
        '',
        '',
        r[mapping['des1']].strip(),
        r[mapping['des2']].strip(),
        drawing,
        unicode(r[mapping['dim']]).strip(),
        r[mapping['remarks']].strip(),
        '',
        '',
        '',
        '',
        '',
        '']


def process_stklst(p, drawing=''):
    """ return the rows of interest (E-rows)
    """
    book = xlrd.open_workbook(p)
    sheet = book.sheet_by_index(0)

    headers = [c.value.strip() for c in sheet.row_slice(rowx=0)]
    can_map, mapping = get_dyn_mapping(headers)
    if not can_map:
        _logger.warn("Check headers in {}: {}".format(os.path.basename(p), mapping))
        return

    # Obtain mapper function
    mapper = get_dest_mapper(mapping, drawing)

    # Loop over rows, filter, map and yield
    for n in range(1, sheet.nrows):
        l = sheet.row_slice(rowx=n)
        stktyp = l[mapping['type']].value.strip()
        if len(stktyp) > 0 and stktyp[-1] == "E":
            yield mapper([c.value for c in l])


class NFETeileProcess():
    def __init__(self, logger=None):
        self.logger = logger

        self.basedir = ""
        self.DIR_STKS = ""
        self.DIR_OUT = ""
        self.DIR_ZEICHN = ""
        self.input_file = ""
        self.stklsts = []
        self.outlines = []

        self.src_startrow = 2
        self.write_header = True
        self.subheaders = True
        self.skip_empty_subheaders = True

    def set_dir_setup(self, dirset):
        self.DIR_OUT = dir_setup[dirset]['out']
        self.DIR_STKS = dir_setup[dirset]['stks']
        self.DIR_ZEICHN = dir_setup[dirset]['zeichn']

    def set_basedir(self, bdir):
        self.basedir = bdir

    def dirchecks(self):
        """ Test if dirs ok """
        if not os.path.isdir(os.path.join(self.basedir, self.DIR_OUT)):
            self.logger.warn("Directories not found : expecting '{}', '{}' and '{}' in {}".format(self.DIR_OUT, self.DIR_STKS, self.DIR_ZEICHN, self.basedir))
            return False
        return True

    def detect_mainfile(self):
        """" determine main file """
        mainfiles = []
        self.input_file = ""
        allowed_extensions = ['xls', 'xlsx']
        for f in os.listdir(os.path.join(self.basedir, self.DIR_ZEICHN)):
            if f.split('.')[-1] in allowed_extensions:
                mainfiles.append(f)

        if len(mainfiles) != 1:
            self.logger.warn(
                "Don't know which file to parse - please check directory or specify a filename")
            return ""

        self.input_file = mainfiles[0]
        return self.input_file

    def find_stklsts(self):
        self.stklsts = list(gather_stklsts(os.path.join(self.basedir, self.DIR_STKS)))


    def get_outrows(self):
        """ Open the drawings file, match with stklists, filter and map rows """

        # Open main file, use first sheet
        book = xlrd.open_workbook(os.path.join(self.basedir, self.DIR_ZEICHN, self.input_file))
        sheet = book.sheet_by_index(0)

        # Build output here
        self.outlines = []

        # Parse rows
        # attempt to match drawing / file
        # if match, get mapped rows
        # add mapped rows and optional drawing-header rows
        for n in range(self.src_startrow, sheet.nrows):
            row = sheet.row_slice(rowx=n, start_colx=0, end_colx=6)
            handle = row[0].value.strip()
            drawing = row[1].value.strip()
            if handle:
                fname = match_stklst(handle, self.stklsts)
                if not fname:
                    self.logger.warn("File missing: {}, {} (pos {})".format(handle, drawing, n))
                    continue

                # Child-rows (ersatzteile)
                childrows = list(process_stklst(os.path.join(self.basedir, self.DIR_STKS, fname), drawing))

                # Add drawing sub-header
                if self.subheaders and (len(childrows) > 0 or not self.skip_empty_subheaders):
                    self.outlines.append(['', '', '', '', '', drawing, handle])

                # Append teile rows
                for l in childrows:
                    self.outlines.append(l)

    def write_outrows(self):
        """ Write rows to output """
        dest = os.path.join(self.basedir, self.DIR_OUT, 'out.csv')
        with open(dest, 'wb') as outfile:
            wr = UnicodeWriter(outfile, delimiter=';', quotechar='"')

            if self.write_header:
                wr.writerow(outheaders)

            for r in self.outlines:
                wr.writerow([unicode(v) for v in r])

        self.logger.info("Done, written to {}".format(dest))

dir_setup = {'umlaut': {'out': "Ersatz- und Verschleißteillisten",
                        'stks': "Stücklisten Mechanik",
                        'zeichn' : "Zeichnungen Mechanik"},
             'simple': {'out': "teile",
                        'stks': "stklst",
                        'zeichn' : "zeichnung"},
}

outheaders = [u'Pos.', u'Stück zahl', u'Best. ein heit', u'Ges. stück zahl', u'Benennung',
              u'Typ- und Normbez. Werkstoff', u'Zeichn. Nr.', u'Bestell. u. Serien Nr. Abmessungen',
              u'Hersteller', u'Ersatz teil Menge', u'Ver- schleiß-teil Menge', u'Liefer-zeit in Wo.',
              u'Preis (netto) in Euro', u'ges. Preis (netto) in Euro']


class TextHandler(logging.Handler):
    """This class allows you to log to a Tkinter Text or ScrolledText widget"""
    def __init__(self, text):
        # run the regular Handler __init__
        logging.Handler.__init__(self)

        # Store a reference to the Text it will log to
        self.text = text

    def emit(self, record):
        msg = self.format(record)
        def append():
            self.text.configure(state='normal')
            self.text.insert(Tkinter.END, msg + '\n')
            self.text.configure(state='disabled')
            # Autoscroll to the bottom
            self.text.yview(Tkinter.END)
        # This is necessary because we can't modify the Text from other threads
        self.text.after(0, append)


class TkFileDialogExample(Tkinter.Frame):

    def __init__(self, root, logger=None):

        Tkinter.Frame.__init__(self, root)

        # options for buttons
        button_opt = {'fill': Tkconstants.BOTH, 'padx': 5, 'pady': 5}

        # define buttons
        Tkinter.Button(self, text='Choose directory', command=self.askdirectory).pack(**button_opt)

        # defining options for opening a directory
        self.dir_opt = options = {}
        options['initialdir'] = 'C:\\'
        options['mustexist'] = False
        options['parent'] = root
        options['title'] = 'Choose directory...'

        self.logger = logger

    def askdirectory(self):
        """Returns a selected directoryname."""
        theDir = tkFileDialog.askdirectory(**self.dir_opt)
        self.logger.info(theDir)
        self.do_process(theDir)
        return theDir

    def do_process(self, basedir):
        p = NFETeileProcess(logging.getLogger())
        p.set_basedir(basedir)
        p.set_dir_setup('simple')

        # p.src_startrow = args.startrow if args.startrow != None else 2
        # p.write_header = True if args.headers else False
        # p.subheaders = False if args.drawingheader == 'skip' else True
        # p.skip_empty_subheaders = True if args.drawingheader == 'nonempty' else False

        if not p.dirchecks():
            return False

        if not p.detect_mainfile():
            return False

        p.find_stklsts()
        p.get_outrows()
        p.write_outrows()
        return True

if __name__=='__main__':
    root = Tkinter.Tk()
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger()

    TkFileDialogExample(root, logger).pack()

    st = ScrolledText.ScrolledText(root, state='disabled')
    st.configure(font='TkFixedFont')
    st.pack()

    # Create textLogger + Add the handler to logger
    text_handler = TextHandler(st)
    text_handler.setLevel(logging.DEBUG)
    logger.addHandler(text_handler)

    root.mainloop()
